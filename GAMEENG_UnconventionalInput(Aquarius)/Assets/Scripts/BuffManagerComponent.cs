﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManagerComponent : MonoBehaviour
{
    [SerializeField] private Unit owner;

    [SerializeField] private List<BuffEffects> listOfBuffs = new List<BuffEffects>();

    public void Init(Unit nOwner)
    {
        owner = nOwner;
        GameEvents.eventSystem.onBuffEnd += RemoveEndedBuffFromList;
    }

    public BuffEffects GetBuffReference(string buffName)
    {
        foreach(BuffEffects buff in listOfBuffs)
        {
            if (buffName == buff.GetBuffName())
            {
                return buff;
            }
        }

        return null;
    }

    private void RemoveEndedBuffFromList(BuffEffects nBuff) // EVERYTHING else.from buffs who destroy themselves and then send an event message 
    {
        //foreach (BuffEffects buff in listOfBuffs)
        //{
        //    if (nBuff.GetBuffName() == buff.GetBuffName())
        //    {
                listOfBuffs.Remove(nBuff); // remove it from the list. normally, this isnt needed as it's destroyed and dereferenced but this is object pooling 
        //    }
        //}
        Debug.Log(nBuff.name + " got removed from the list player's buffs (BUFF MANAGER)");
    }

    public void EndBuff(string buffName) // USED by contact obstacles such as SLOW and EMP. 
    {
        foreach (BuffEffects buff in listOfBuffs)
        {
            if (buffName == buff.GetBuffName())
            {
                buff.EndBuff();
                Debug.Log("BUFF MANAGER CALLED TO END " + buff.name);
                return;
               // listOfBuffs.Remove(buff); // remove it from the list. normally, this isnt needed as it's destroyed and dereferenced but this is object pooling
               // commented ^ above because calling EndBuff() will call an event anyway which RemoveEndedBuffFromList listens to
            }
        }
    }

    public void SpawnBuff(BuffEffects theBuff)
    {
        // spawn the buff
        BuffEffects newBuff = Instantiate(theBuff, transform.position, Quaternion.identity);

        // activate it 
        newBuff.Init(owner.GetComponent<Player>());
        newBuff.ActivateBuff();

        // add the spawned buff to the list of buffs managed by this component 
        listOfBuffs.Add(newBuff);

        Debug.Log(newBuff.name + " got added to the list of player's b uffs (BUFF MANAGER)");
    }

}
