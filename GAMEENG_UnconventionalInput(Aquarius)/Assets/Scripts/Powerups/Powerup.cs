﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : BuffImplementor
{
    public override void ApplyBuff()
    {
        base.ApplyBuff();

        GameEvents.eventSystem.PowerupTappedOrReachedCollider();
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("TileCollider"))
        {
            Debug.Log(this.name + " hit tile collider, removing");
            GameEvents.eventSystem.PowerupTappedOrReachedCollider();
            gameObject.SetActive(false);
        }
    }
}
