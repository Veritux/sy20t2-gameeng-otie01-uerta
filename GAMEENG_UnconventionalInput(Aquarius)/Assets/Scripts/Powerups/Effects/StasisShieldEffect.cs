﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StasisShieldEffect : BuffEffects
{
    public override void ActivateBuff()
    {
        Debug.Log("shield effect actiavatefd");
        target.SetCanTakeDamage(false);
    }

    protected override void DeactivateBuff()
    {
        Debug.Log("shield effect deadctivadka");
        target.SetCanTakeDamage(true);
    }
}
