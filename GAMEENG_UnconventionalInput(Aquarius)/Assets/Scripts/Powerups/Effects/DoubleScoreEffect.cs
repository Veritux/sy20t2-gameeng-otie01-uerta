﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScoreEffect : BuffEffects
{
    public override void ActivateBuff()
    {
        Debug.Log("2x score activaed");
        int currentBaseAddMulti = ScoreManager.instance.GetBaseAdditionalMultiplier();
        float newBaseAddMulti = currentBaseAddMulti * buffStrength;
        Debug.Log("the new base additional multi to pass (SHOULD BE 2): " + newBaseAddMulti);
        ScoreManager.instance.ModifyBaseAdditionalMultiplier((int)newBaseAddMulti);
    }

    protected override void DeactivateBuff()
    {
        Debug.Log("2x score deactivaddd");
        int currentBaseAddMulti = ScoreManager.instance.GetBaseAdditionalMultiplier();
        float newBaseAddMulti = currentBaseAddMulti / buffStrength;
        Debug.Log("the new base additional (SHOULD BE BACK TO 1) multi to pass: " + newBaseAddMulti);
        ScoreManager.instance.ModifyBaseAdditionalMultiplier((int)newBaseAddMulti);
    }
}
