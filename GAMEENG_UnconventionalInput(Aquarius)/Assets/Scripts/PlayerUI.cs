﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private GameObject ScoreText;
    [SerializeField] private GameObject ComboText;
    [SerializeField] private GameObject HealthText;
    [SerializeField] private GameObject playerRef;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.GetComponent<Text>().text = "Score: " + ScoreManager.instance.GetScore();
        ComboText.GetComponent<Text>().text = "Multiplier: " + ScoreManager.instance.GetCurrentMultiplier();
        HealthText.GetComponent<Text>().text = "Health: " + playerRef.GetComponent<Player>().GetHealthComponent().GetCurrentHP();
    }
}
