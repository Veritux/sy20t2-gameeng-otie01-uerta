﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundTile : MonoBehaviour
{
    [SerializeField] Transform nextWallLocation;
    [SerializeField] private float lifetime;
    [SerializeField] private float tileSpeed;

    public Transform GetNextWallLocation() { return nextWallLocation; }
    public void despawn()
    {
        GameEvents.eventSystem.BackgroundTileDestroyed(this);
        gameObject.SetActive(false);

        // Destroy(this.gameObject); // maybe object pooling instead? idk this is temporary then
    }

    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frameW
    void Update()
    {
        // continually move downwards
        this.transform.Translate((Vector3.left) * tileSpeed * Time.deltaTime);
    }
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
       // Debug.Log("Collided with something!");
        if (collision.gameObject.CompareTag("TileCollider"))
        {
            StartCoroutine(DespawnAfterDelay(3));
        }
    }

    IEnumerator DespawnAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);

        despawn();
    }
}
