﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float smooth;
    [SerializeField] private float baseSensitivity;
    [SerializeField] Rigidbody2D playerRigidBody;

    private Vector3 currentAcceleration, initialAcceleration;
    private float sensitivity;
    private float processedXForce;

    public float GetSensitivity()
    {
        return sensitivity;
    }

    public void SetSensitivity(float value)
    {
        sensitivity = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        initialAcceleration = Input.acceleration;
        currentAcceleration = Vector3.zero;
        sensitivity = baseSensitivity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        currentAcceleration = Vector3.Lerp(currentAcceleration, Input.acceleration - initialAcceleration, Time.deltaTime / smooth);
        processedXForce = Mathf.Clamp(currentAcceleration.x * sensitivity, -5, 5);
        Vector3 thisAcceleration = new Vector3(processedXForce, 0, 0);
        playerRigidBody.AddForce(thisAcceleration);

       // playerRigidBody.MovePosition(transform.position + thisAcceleration);
    }
}
