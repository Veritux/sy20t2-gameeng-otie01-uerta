﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : Spawnable
{
    protected HealthComponent healthComponent;

    protected virtual void Start()
    {
        healthComponent = GetComponent<HealthComponent>();
    }

    public void SetCanTakeDamage(bool value)
    {
        healthComponent.SetCanTakeDamage(value);
    }

    public HealthComponent GetHealthComponent()
    {
        return healthComponent;
    }

    public virtual void Die()
    {

    }
}
