﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchListener : MonoBehaviour
{
    [SerializeField] private float minimumSwipeDistance;

    private Vector2 startPos, swipeDelta, latestPos;
    private bool isDragging = false;
    private bool isSwiping = false;

    private void Reset()
    {
        // Reset is only called whenever a key is released
        if (isSwiping == false)
        {
            Debug.Log("Tap detected");
#if UNITY_EDITOR
            GameEvents.eventSystem.Tap(Input.mousePosition);
#elif UNITY_ANDROID
            GameEvents.eventSystem.Tap(Input.touches[0].position);
#endif
        }
        else
        {
            Debug.Log("Swipe ended");
            GameEvents.eventSystem.SwipeEnd();
        }

        startPos = Vector2.zero;
        swipeDelta = Vector2.zero;
        isDragging = false;
        isSwiping = false;
    }

    private void Update()
    {
#if UNITY_EDITOR
        mouseUpdate();
#elif UNITY_ANDROID
        touchUpdate();
#endif
        if (isSwiping == false)
        {
            calculateSwipe();
        }

        if (isSwiping == true)
        {
            GameEvents.eventSystem.StillSwiping(latestPos);
        }
    }

    void mouseUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Record initial touch position
            isDragging = true;
            isSwiping = false;
            startPos = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            isDragging = true;
            latestPos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
            Reset();
        }
    }

    void touchUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    // Record initial touch position.
                    isDragging = true;
                    isSwiping = false;
                    startPos = touch.position;
                    break;
                case TouchPhase.Moved:
                    isDragging = true;
                    latestPos = touch.position;
                    break;
                case TouchPhase.Ended:
                    isDragging = false;
                    Reset();
                    break;

                case TouchPhase.Canceled:
                    isDragging = false;
                    Reset();
                    break;
            }
        }
    }

    private void calculateSwipe()
    {
        // Calculating distance
        swipeDelta = Vector2.zero; // resetting per frame
        if (isDragging)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButton(0)) // If left mouseButton is still held
            {
                swipeDelta = (Vector2)Input.mousePosition - startPos;
            }
#elif UNITY_ANDROID
            if (Input.touchCount > 0)
            {
                swipeDelta = Input.touches[0].position - startPos; // In case there of multiple touches, just use the first 
            }
#endif
        }

        // check if minimum swipe has been reached
        if (swipeDelta.magnitude > minimumSwipeDistance)
        {
            Debug.Log("Swipe detected!");
            isSwiping = true;
            GameEvents.eventSystem.SwipeStart(startPos);
        }
    }
}
