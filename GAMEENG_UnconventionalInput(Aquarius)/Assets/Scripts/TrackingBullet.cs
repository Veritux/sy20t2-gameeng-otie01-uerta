﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingBullet : Bullet
{
    private Unit target;

    public override void Init(int nDamage, Unit nOwner, Unit nTarget)
    {
        base.Init(nDamage, nOwner, nTarget);

        target = nTarget;
    }

    protected override void Movement()
    {
        // tracking stuff using target
        MoveTowardsPlayer();
    }

    // recycling code from Enemy_Collider. could actually make a movement component out of this instead. 
    private void MoveTowardsPlayer()
    {
        Transform goal = target.transform;
        Vector2 direction = goal.position - transform.position;

        transform.position = Vector3.MoveTowards(transform.position, goal.position, bulletSpeed * Time.deltaTime);
        //this.transform.position = Vector3.Lerp(transform.position, goal.transform.position, Time.deltaTime * speed);
    }
}
