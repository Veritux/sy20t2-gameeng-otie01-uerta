﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : Spawner
{
    [SerializeField] private Transform shooterMoveTarget; // for enemy spawn manager only

    protected override void Start()
    {
        base.Start();
        GameEvents.eventSystem.onEnemyReachedTileCollider += DecrementSpawnedSpawnables;
    }

    protected override void InitJustSpawnedObject()
    {
        // dirty code reeeee
        if (justSpawnedSpawnable.GetComponent<Enemy_Shooter>())
        {
            justSpawnedSpawnable.GetComponent<Enemy_Shooter>().SetMoveForwardTarget(shooterMoveTarget);
        }

        // Init
        justSpawnedSpawnable.GetComponent<Enemy>().Init(playerReference);
    }
}
