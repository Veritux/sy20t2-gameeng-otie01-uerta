﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawnManager : Spawner
{
    protected override void Start()
    {
        base.Start();
        GameEvents.eventSystem.onObstacleReachedTileCollider += DecrementSpawnedSpawnables;
    }

    protected override void InitJustSpawnedObject()
    {

        // Init
        justSpawnedSpawnable.GetComponent<Obstacles>().Init(playerReference);
    }
}
