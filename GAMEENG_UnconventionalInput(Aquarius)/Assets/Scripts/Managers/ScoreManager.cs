﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance = null; // Singleton 

    private int currentScore = 0;
    [SerializeField] private int scoreGrowthFactor;
    private int baseMultiplier = 1;
    private int baseAdditionalMultiplier = 1; // For the purposes of the Double Score Effect 
    private int additionalMultiplier = 1;
    private int currentMultiplier = 1;
    private float timeSinceScoreIncreased = 0;
    [SerializeField] private float timeForScoreComboIncrease;
    [SerializeField] private int maxAdditionalComboMultiplier;
    [SerializeField] private float scoreIncreaseInterval;
    private List<DoubleScoreEffect> doubleScoreEffectActiveList = new List<DoubleScoreEffect>();

    private bool isPlayerAlive = true;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentMultiplier = baseMultiplier * baseAdditionalMultiplier * additionalMultiplier;
        InvokeRepeating("UpdateScore", scoreIncreaseInterval, scoreIncreaseInterval);

        // Listen to events
        GameEvents.eventSystem.onEnemyDestroyed += UpdateScoreKilledEnemy;
        GameEvents.eventSystem.onPlayerTakeDamage += ResetScoreMulti;
        GameEvents.eventSystem.onPlayerDeath += UpdatePlayerStatus;
    }

    private void Update()
    {
        timeSinceScoreIncreased += Time.deltaTime;    
    }

    public void ModifyAdditionalScoreMultiplier(int value)
    {
        Debug.Log("Moidified additionalScoreMultiplier to: " + value);
        additionalMultiplier = value;
    }

    #region Getter Functions
    public int GetScore()
    {
        return currentScore;
    }

    public int GetCurrentMultiplier()
    {
        currentMultiplier = baseMultiplier * baseAdditionalMultiplier * additionalMultiplier;
        return currentMultiplier;
    }

    public int GetBaseAdditionalMultiplier()
    {
        return baseAdditionalMultiplier;
    }
    #endregion

    #region DoubleScorePowerup Stuff
    public void AddToDoubleScoreActiveList(DoubleScoreEffect prefab)
    {
        Debug.Log("Adding a DoubleScoreEffectPrefab");
        doubleScoreEffectActiveList.Add(prefab);
    }

    public void RemoveFromDoubleScoreActiveList(DoubleScoreEffect prefab)
    {
        Debug.Log("Removing a DoubleScoreEffectPrefab");
        doubleScoreEffectActiveList.Remove(prefab);
    }

    public bool IsThereOtherDoubleScoreActive()
    {
        if (doubleScoreEffectActiveList.Count > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    /* The score multiplier doubles every 30 seconds from 1x up to a maximum of 16x. 
    Every hit taken by the player will halve the score multiplier */

    void UpdateScore()
    {
        // Every "interval", add score
        if (isPlayerAlive == true)
        {
            if (timeSinceScoreIncreased >= timeForScoreComboIncrease)
            {
                additionalMultiplier *= 2;
                timeSinceScoreIncreased = 0; // Reset 
            }

            if (additionalMultiplier >= maxAdditionalComboMultiplier)
            {
                additionalMultiplier = maxAdditionalComboMultiplier;
            }

            currentMultiplier = baseMultiplier * baseAdditionalMultiplier * additionalMultiplier; // baseAdditionalMultiplier is only modified by picking up a 2x powerup

            currentScore += (scoreGrowthFactor * currentMultiplier);
        }
    }

    private void UpdateScoreKilledEnemy(int scoreValue)
    {
        // Called whenever an enemy is Killed
        currentMultiplier = baseMultiplier * baseAdditionalMultiplier * additionalMultiplier;
        currentScore += (scoreValue * currentMultiplier);
    }

    public void ModifyBaseAdditionalMultiplier(int x)
{
        // For DOUBLE SCORE EFFECT. Use the same function to disable it 
        baseAdditionalMultiplier = x;

        if (baseAdditionalMultiplier <= 0)
        {
            baseAdditionalMultiplier = 1;
        }

        Debug.Log("ModifyBasedAdditionalMulti called. base add multi is now: " + baseAdditionalMultiplier);
        Debug.Log("BaseMutli: " + baseMultiplier + ", baseAddMulti: " + baseAdditionalMultiplier + ", additionalMultil: " + additionalMultiplier);
        currentMultiplier = baseMultiplier * baseAdditionalMultiplier * additionalMultiplier;
        Debug.Log("curent score mutliper is now: " + currentMultiplier);
}

private void ResetScoreMulti()
{
    // Halve the score multiplier by 2 when hit 
    additionalMultiplier /= 2;

        if (additionalMultiplier <= 0)
        {
            additionalMultiplier = 1;
        }
}

private void UpdatePlayerStatus()
{
    isPlayerAlive = false;
}
}
