﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : Spawner
{
    protected override void Start()
    {
        base.Start();
        GameEvents.eventSystem.onPowerupTappedOrReahedCollilder += DecrementSpawnedSpawnables;
    }


    protected override void InitJustSpawnedObject()
    {
        // Init
        if (justSpawnedSpawnable == null)
        {
            Debug.Log("Just spawned spawnable == null");
        }
        justSpawnedSpawnable.GetComponent<Powerup>().Init(playerReference);
    }
}
