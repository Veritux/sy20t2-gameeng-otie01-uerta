﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpawnManager : MonoBehaviour
{
    [SerializeField] private BackgroundTile backgroundTile; // the prefab
    [SerializeField] private Transform tileSpawnLocation; // the initial wall location

    // Start is called before the first frame update
    void Start()
    {
        // Subscribe to the event system (tell this to listen)
        GameEvents.eventSystem.onBackgroundTileDestroyed += OnTileDestroyed;

        // Spawn the initial two walls 
        for (int i = 0; i < 2; i++)
        {
            SpawnWall();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTileDestroyed(BackgroundTile tile)
    {
        SpawnWall();
    }

    private void SpawnWall()
    {
        //// Spawn a wall tile
        //BackgroundTile currentTile = Instantiate(backgroundTile, tileSpawnLocation.position, tileSpawnLocation.transform.rotation); 
        //tileSpawnLocation = currentTile.getNextWallLocation();

        // Trying out object pooling
        GameObject currentTile = ObjectPooler.instance.SpawnFromPool(backgroundTile.name, tileSpawnLocation.position, tileSpawnLocation.transform.rotation);
        tileSpawnLocation = currentTile.GetComponent<BackgroundTile>().GetNextWallLocation();
    }
}
