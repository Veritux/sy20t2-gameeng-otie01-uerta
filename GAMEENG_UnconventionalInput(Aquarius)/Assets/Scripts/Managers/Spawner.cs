﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject SpawnZone;

    [Header("Spawner Settings")]
    [SerializeField] List<Spawnable> spawnablePrefabs = new List<Spawnable>(); // each spawner needs a different list 
    [SerializeField] private float minSpawnInterval;
    [SerializeField] private float maxSpawnInterval;
    [SerializeField] private int maxNoSpawnables;
    [SerializeField] protected Player playerReference;

    protected GameObject justSpawnedSpawnable;

    private int currentSpawnables;
    private float spawnInterval;
    private ObjectPooler objectPooler;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        objectPooler = ObjectPooler.instance;

        spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
        InvokeRepeating("SpawnerBehavior", 5.0f, spawnInterval);

        GameEvents.eventSystem.onPlayerDeath += StopSpawner;
    }

    protected void DecrementSpawnedSpawnables()
    {
        currentSpawnables--;
    }

    private void StopSpawner()
    {
        CancelInvoke("SpawnerBehavior");
    }

    protected virtual void SpawnSpawnable(Vector3 spawnLocation, int randomIndex)
    {
        // Object pooling spawn
        justSpawnedSpawnable = objectPooler.SpawnFromPool(spawnablePrefabs[randomIndex].name, spawnLocation, Quaternion.identity);
        InitJustSpawnedObject();
    }

    protected virtual void InitJustSpawnedObject()
    {

    }

    private void SpawnerBehavior()
    {
        if (currentSpawnables >= maxNoSpawnables)
        {
            return;
        }

        int randomIndex = Random.Range(0, spawnablePrefabs.Count);
        Vector2 bounds;
        Vector3 spawnLocation = new Vector3(0, 0, playerReference.transform.position.z);

        bounds = SpawnZone.GetComponent<BoxCollider2D>().bounds.size;
        bounds.x /= 2;
        bounds.y /= 2;

        spawnLocation.x = SpawnZone.transform.position.x + Random.Range(-bounds.x, bounds.x);
        spawnLocation.y = SpawnZone.transform.position.y + Random.Range(-bounds.y, bounds.y);

        SpawnSpawnable(spawnLocation, randomIndex);

        currentSpawnables++;

        spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
    }
}
