﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Player player;

    public static GameManager instance = null; // Singleton 

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.eventSystem.onPlayerDeath += PlayerDied;
    }

    private void PlayerDied()
    {
        // game over stuff here 
    }
}
