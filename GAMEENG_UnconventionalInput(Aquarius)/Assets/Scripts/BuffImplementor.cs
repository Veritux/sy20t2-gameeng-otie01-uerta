﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffImplementor : Spawnable
{
    [SerializeField] protected BuffEffects buff;
    [SerializeField] protected float moveDownwardSpeed;

    protected Player playerReference;

    public void Init(Player playerRef)
    {
        playerReference = playerRef;
    }

    public virtual void ApplyBuff()
    {
        if (playerReference.GetBuffManagerComponent().GetBuffReference(buff.GetBuffName()) != null)
        {
            // If the buff is already present in the player 
            Debug.Log(buff.name + " is already present in the player, telling it to refresh");
            playerReference.GetBuffManagerComponent().GetBuffReference(buff.GetBuffName()).RefereshDuration();
        }
        else
        {
            // that player doesn't have a buff yet so call the buff manager component to spawn said buff 
            Debug.Log(buff.name + " is NOT present in the player, telling its buff manager to spawn said buff");
            playerReference.GetBuffManagerComponent().SpawnBuff(buff);
        }
    }

    protected virtual void EndBuff()
    {
        // only used by contact obstacles
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        // continously going down 
        this.transform.Translate((-Vector3.up) * moveDownwardSpeed * Time.deltaTime);
    }
}
