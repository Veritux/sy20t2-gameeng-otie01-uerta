﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : Unit
{
    [Header("Player Settings")]
    [SerializeField] private Bullet bulletPrefab;

    [SerializeField] private float shootingSpeed;
    [SerializeField] private int bulletDamage;
    [SerializeField] private float delayHealthRegen;

    [SerializeField] private BuffManagerComponent buffManagerComponent;
    private PlayerController controllerReference;

    [Header("Superweapon Settings")]
    [SerializeField] private SuperWeapon superWeaponPrefab;
    [SerializeField] float superWeaponYOffset;
    private SuperWeapon superWeaponReference;

    private bool superWeaponActive = false;
    private bool isShooting = false;
    private bool canShoot = true;
    private bool isAlive = true; // don't really make much use of this variable in this instance

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        controllerReference = GetComponent<PlayerController>();

        buffManagerComponent.Init(this); // give owner reference
        InvokeRepeating("Attack", 0f, shootingSpeed);
        InvokeRepeating("RegenHealth", delayHealthRegen, delayHealthRegen);

        isShooting = true;
    }

    public void ActivateSuperWeapon()
    {
        superWeaponActive = true;
        CancelInvoke("Attack");
        Vector3 spawnPosition = transform.position;
        spawnPosition.y += superWeaponYOffset;
        superWeaponReference = Instantiate(superWeaponPrefab, spawnPosition, Quaternion.identity, this.transform);
    }

    public void DeactivateSuperWeapon()
    {
        superWeaponActive = false;
        InvokeRepeating("Attack", 1.0f, shootingSpeed);
        Destroy(superWeaponReference.gameObject);

        Debug.Log("Superweapon should not be here anymore");
    }

    public PlayerController GetPlayerControllerReference()
    {
        return controllerReference;
    }

    public BuffManagerComponent GetBuffManagerComponent()
    {
        return buffManagerComponent;
    }

    public void SetCanShoot(bool value)
    {
        Debug.Log("can shoot value modified!");
        canShoot = value;
    }

    private void RegenHealth()
    {
        healthComponent.RestoreHealth(1);
    }

    private void Attack()
    {
        if (canShoot == true)
        {
            Quaternion spawnRotation = transform.rotation * Quaternion.Euler(1, 1, /*-90*/ 1);

            GameObject newBullet = ObjectPooler.instance.SpawnFromPool(bulletPrefab.name, transform.position, spawnRotation);
            newBullet.GetComponent<Bullet>().Init(bulletDamage, this, null);
            newBullet.GetComponent<Bullet>().SetDirection(1); // setting this again cuz we are recycling bulletes whose values have been changed by enemies 
        }
        else
        {
            Debug.Log("Player cannot shoot!");
        }
    }

    public override void Die()
    {
        if (isAlive == true)
        {
            // Player death stuff
            Debug.Log("Player is out of health");
            GameEvents.eventSystem.PlayerDeath();
            CancelInvoke("Attack");
            CancelInvoke("RegenHealth");
            // isAlive = false;
            SceneManager.LoadScene("Game Over");
        }
    }
}
