﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private int maxHP;
    [SerializeField] private int currentHP;

    private bool canTakeDamage = true;

    public void SetCanTakeDamage(bool value)
    {
        canTakeDamage = value;
    }

    public int GetMaxHP()
    {
        return maxHP;
    }

    public int GetCurrentHP()
    {
        return currentHP;
    }

    public void ResetHealth()
    {
        currentHP = maxHP; // called when reactiavting stuff etc. 
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }

    public void RestoreHealth(int value)
    {
        Debug.Log(this.name + " restored " + value + " health");
        currentHP += value;
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }
    }

    public void TakeDamage(int damageValue)
    {
        if (canTakeDamage == true)
        {
            Debug.Log(this.name + " received" + damageValue + " damage");
            currentHP -= damageValue;
            if (currentHP <= 0)
            {
                // Call die from owner
                transform.gameObject.GetComponent<Unit>().Die();
            }
        }
    }
}
