﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : BuffImplementor
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log(this.name + "hit the player, applying buff");
            ApplyBuff();
        }

        else if (collision.gameObject.CompareTag("TileCollider"))
        {
            Debug.Log(this.name + " hit tile collider, removing");
            GameEvents.eventSystem.ObstacleReachedTileCollider();
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log(this.name + "obstacle exited the player, ending buff");
            EndBuff();
        }
    }
}
