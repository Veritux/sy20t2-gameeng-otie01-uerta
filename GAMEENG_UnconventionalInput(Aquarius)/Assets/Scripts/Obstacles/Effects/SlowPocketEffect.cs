﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowPocketEffect : BuffEffects
{
    public override void ActivateBuff()
    {
        Debug.Log("SLOW  effect acatived");
        float currentSensitivity = target.GetPlayerControllerReference().GetSensitivity();
            float newSensitivity = currentSensitivity * buffStrength;
            target.GetPlayerControllerReference().SetSensitivity(newSensitivity);

    }

    protected override void DeactivateBuff()
    {
        Debug.Log("SLOW effect deeeeeeacatived");
        float currentSensitivity = target.GetPlayerControllerReference().GetSensitivity();
        float newSensitivity = currentSensitivity / buffStrength;
        target.GetPlayerControllerReference().SetSensitivity(newSensitivity);
    }
}
