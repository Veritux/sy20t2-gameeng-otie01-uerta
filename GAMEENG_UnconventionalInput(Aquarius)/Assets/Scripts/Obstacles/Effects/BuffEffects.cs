﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffEffects : MonoBehaviour
{
    [SerializeField] private string buffName;
    [SerializeField] protected float buffDuration;
    [SerializeField] protected float buffStrength;

    [SerializeField] protected float currentBuffDuration;

    protected Player target;

    public string GetBuffName()
    {
        return buffName;
    }

    public void RefereshDuration()
    {
        Debug.Log(this.name + " was told to refresh duration");
        currentBuffDuration = buffDuration;
    }

    public void Init(Player theTarget)
    {
        target = theTarget;
        InvokeRepeating("DecrementCurrentBuffDuration", 0, 1.0f);
        currentBuffDuration = buffDuration;
    }

    public void EndBuff()
    {
        Debug.Log("End buff called: " + this.name);
        CancelInvoke("DecrementCurrentBuffDuration");
        DeactivateBuff();
        GameEvents.eventSystem.BuffEnd(this);
        // gameObject.SetActive(false);
        Destroy(gameObject);
    }

    private void DecrementCurrentBuffDuration()
    {
        Debug.Log(this.name + "current buff duration: " + currentBuffDuration);
        currentBuffDuration--;
                Debug.Log(this.name + "current buff duration after the decrement was called: " + currentBuffDuration);
        if (currentBuffDuration <= 0)
        {
            Debug.Log(this.name + "current buff duration is <= 0 so telling it to end ");
            EndBuff();
        }
    }

    protected virtual void DeactivateBuff()
    {
        // basically stuff to do before "despawning" the buff
    }

    public virtual void ActivateBuff() // called by the buff manager component which spawned it 
    {
        // to be overriden 
    }

    // Update is called once per frame
    void Update()
    {

    }
}
