﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidPocketEffect : BuffEffects
{

    [SerializeField] private float buffTickRate;

    public override void ActivateBuff()
    {
        currentBuffDuration = buffDuration;
        InvokeRepeating("TickEffect", 0.5f, buffTickRate);
    }

    protected override void DeactivateBuff()
    {
        CancelInvoke("DoTickEffect");
    }

    private void TickEffect()
    {
       target.GetHealthComponent().TakeDamage((int)buffStrength);
    }
}
