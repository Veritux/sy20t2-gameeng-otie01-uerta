﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactOnlyObstacle : Obstacles
{
    protected override void EndBuff()
    {
        playerReference.GetBuffManagerComponent().EndBuff(buff.GetBuffName());
    }
}
