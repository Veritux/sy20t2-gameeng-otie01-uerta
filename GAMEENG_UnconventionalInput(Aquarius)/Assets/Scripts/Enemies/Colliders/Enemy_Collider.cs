﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Collider : Enemy
{
    protected override void Movement()
    {
        // Put the base movement here which is to move towards the player. To be used by Normal Rusher and Tank. 
        // When we think of it this way we don't even need the Tank and NormalRusher class. Can be just 2 different prefabs of Enemy_Collider type... 
        // cuz the only thing that differs from them is sprite, speed, and damage. that's all. 
        RotateTowardsPlayer();
        MoveTowardsPlayer();
    }

    private void MoveTowardsPlayer()
    {
        Transform goal = playerReference.transform;
        Vector2 direction = goal.position - transform.position;

        transform.position = Vector3.MoveTowards(transform.position, goal.position, movementSpeed * Time.deltaTime);
        //this.transform.position = Vector3.Lerp(transform.position, goal.transform.position, Time.deltaTime * speed);
    }

    private void RotateTowardsPlayer()
    {
        Transform goal = playerReference.transform;
        Vector2 direction = goal.position - transform.position;

        // 2D rotation
        float rotSpeed = 0.8f;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward); // around the Z axis
        this.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotSpeed);

        //Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        //transform.rotation = rotation;
    }
}
