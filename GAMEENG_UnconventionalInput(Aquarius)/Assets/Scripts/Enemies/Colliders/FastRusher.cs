﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastRusher : Enemy_Collider
{
    //  Travels straight down and will deal damage when it collides with player. 
    protected override void Movement()
    {
        this.transform.Translate((-Vector3.up) * movementSpeed * Time.deltaTime); 
    }
}
