﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Shooter : Enemy
{
    [SerializeField] protected float attackSpeed;
    [SerializeField] protected GameObject bulletPrefab;
    [SerializeField] private Transform MoveForwardTarget; // this probably needs to be find component via spawn manager if we can't set this value via prefab 

    public void SetMoveForwardTarget(Transform moveFwdTarget)
    {
        MoveForwardTarget = moveFwdTarget;
    }

    public override void Init(Player thePlayer)
    {
        base.Init(thePlayer);

        // start shooting timer    
        InvokeRepeating("Fire", 3.0f, attackSpeed); // just some delay before starting the repeating function 

        // Move a bit forward (since these guys spawn off-screen remember)
    }

    //private void MoveForward() 
    //{

    //}

    protected override void Movement()
    {
        while (this.gameObject.transform.position.y >= MoveForwardTarget.position.y)
        {
            //Debug.Log("ENEMY SHOOTER SUPPOSED TO MOVE DOWN");
            this.transform.Translate((-Vector3.up) * movementSpeed * Time.deltaTime);
        }
    }

    protected virtual void Fire()
    {
        // Stationary and NormalShooter will make use of this.  
        // Now that i see it, no need for Stationary and SpecialShooter classes since their behavior is just same.
        // SpecialShooter can just have a different Bullet class which handles the tracking. SPecial shooter just gives it a target. 

        Quaternion spawnRotation = transform.rotation * Quaternion.Euler(1, 1, /*-90*/ 1); // in case we need to rotate stuff around 

        GameObject newBullet = ObjectPooler.instance.SpawnFromPool(bulletPrefab.name, transform.position, spawnRotation);
        newBullet.GetComponent<Bullet>().Init(damage, this, playerReference);
        newBullet.GetComponent<Bullet>().SetDirection(-1); // going downwards 
    }

    public override void Die()
    {
        CancelInvoke("Fire");
        base.Die();
    }
}
