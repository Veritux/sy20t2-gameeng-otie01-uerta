﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalShooter : Enemy_Shooter
{
    //  average in health. They shoot projectiles that travel in a straight line. THey continually move downwards from the top edge of the screen 

    protected override void Movement()
    {
        this.transform.Translate((-Vector3.up) * movementSpeed * Time.deltaTime);
    }
}
