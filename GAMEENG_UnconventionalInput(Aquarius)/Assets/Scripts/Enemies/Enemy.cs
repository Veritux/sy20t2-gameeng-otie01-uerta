﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    [SerializeField] protected float movementSpeed;
    [SerializeField] protected int damage; // collision damage for colliders and bullet damage for shooters 
    [SerializeField] protected int scoreAmount;

    protected Player playerReference = null; // Given by enemy spawn manager
    // private bool isAlive = true;

    public virtual void Init(Player thePlayer)
    {
        playerReference = thePlayer;
    }

    // Update is called once per frame
    void Update()
    {
        ExecuteBehavior();
    }

    protected virtual void ExecuteBehavior()
    {
        Movement();
    }

    protected virtual void Movement()
    {

    }

    protected virtual void Attack()
    {
        Debug.Log( this.name + " attacked!");
    }

    public override void Die()
    {
        // Death stuff
        Debug.Log(this.name + " was destroyed!");
        GameEvents.eventSystem.EnemyDestroyed(scoreAmount);
        healthComponent.ResetHealth();
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // When collided with player 
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log(this.name + " hit the player");
            collision.GetComponent<Player>().GetHealthComponent().TakeDamage(damage);
            GameEvents.eventSystem.EnemyReachedTileCollider();
            gameObject.SetActive(false);
        }
        else if (collision.gameObject.tag == "TileCollider")
        {
            // hit the tile collider
            Debug.Log(this.name + " hit tile collider, removing");
            GameEvents.eventSystem.EnemyReachedTileCollider();
            gameObject.SetActive(false);
        }
    }
}
