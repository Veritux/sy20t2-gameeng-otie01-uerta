﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents eventSystem;

    private void Awake()
    {
        eventSystem = this;
    }

    #region input events
    // Tap
    public event Action<Vector2> onTap;
    public void Tap(Vector2 inputPos) { onTap?.Invoke(inputPos); }

    // Swipe
    public event Action<Vector2> onSwipe;
    public void SwipeStart(Vector2 inputPos) { onSwipe?.Invoke(inputPos); }

    public event Action<Vector2> onStillSwiping;
    public void StillSwiping(Vector2 inputPos) { onStillSwiping?.Invoke(inputPos); }

    public event Action onSwipeEnd;
    public void SwipeEnd() { onSwipeEnd?.Invoke(); }
    #endregion

    #region player events
    // Player death
    public event Action onPlayerDeath;
    public void PlayerDeath() { onPlayerDeath?.Invoke(); }

    // Player Took damage
    public event Action onPlayerTakeDamage;
    public void PlayerTakeDamage() { onPlayerTakeDamage?.Invoke(); }
    #endregion

    // Background Tiles
    public event Action<BackgroundTile> onBackgroundTileDestroyed;
    public void BackgroundTileDestroyed(BackgroundTile tile) { onBackgroundTileDestroyed?.Invoke(tile); }

    // Enemy
    public event Action<int> onEnemyDestroyed;
    public void EnemyDestroyed(int score) { onEnemyDestroyed?.Invoke(score); }

    public event Action onEnemyReachedTileCollider;
    public void EnemyReachedTileCollider() { onEnemyReachedTileCollider?.Invoke(); }

    // Obstacle
    public event Action onObstacleReachedTileCollider;
    public void ObstacleReachedTileCollider() { onObstacleReachedTileCollider?.Invoke(); }

    // PowerUp
    public event Action onPowerupTappedOrReahedCollilder;
    public void PowerupTappedOrReachedCollider() { onPowerupTappedOrReahedCollilder?.Invoke(); }

    // Buffs
    public event Action<BuffEffects> onBuffEnd;
    public void BuffEnd(BuffEffects buff) { onBuffEnd?.Invoke(buff); }
}
