﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected float bulletSpeed;
    [SerializeField] private float lifetime;

    private int damage;
    protected Unit owner = null; // Given the one that spawned the bullet
    private bool isOwnerAnEnemy = false;
    private int direction = 1; // positive (1) for going up, negative (1) for going down

    public void SetDirection(int value)
    {
        // + 1 or -1 only please 
        direction = value;
    }

    public virtual void Init(int nDamage, Unit nOwner, Unit nTarget)
    {
        damage = nDamage;
        owner = nOwner;

        if (nOwner.GetComponent<Enemy>())
        {
            isOwnerAnEnemy = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disableAfterSeconds(lifetime));
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    protected virtual void Movement()
    {
        this.transform.Translate((direction * Vector3.up) * bulletSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // first check that the bullet isn't colliding with the owner mkay.
        if (col.gameObject == owner.gameObject)
        {
           // Debug.Log("Bullet hit the owner owo");
            return;
        }
        if (isOwnerAnEnemy == false && col.gameObject.CompareTag("Enemy"))
        {
            col.gameObject.GetComponent<Enemy>().GetHealthComponent().TakeDamage(damage);
            Debug.Log("Bullet hit Enemy!");
            gameObject.SetActive(false);
        }
        else if (isOwnerAnEnemy == true && col.gameObject.CompareTag("Player"))
        {
            col.gameObject.GetComponent<Player>().GetHealthComponent().TakeDamage(damage);
            GameEvents.eventSystem.PlayerTakeDamage();
            Debug.Log("BULLET hit PLAYER");
            gameObject.SetActive(false);
        }
    }

    IEnumerator disableAfterSeconds(float lifetime)
    {
        // Debug.Log("I thought this wouldn't work since Start() is callig it even though we spawend this via object pooling");
        yield return new WaitForSeconds(lifetime);
        Debug.Log("Bullet passed it's lifetime, deactivating");
        gameObject.SetActive(false);
    }
}
