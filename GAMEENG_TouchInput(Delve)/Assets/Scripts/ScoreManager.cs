﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance = null; // Singleton 

    private int currentScore = 0;
    [SerializeField] private int scoreGrowthFactor = 10;
    private int baseMultiplier = 1;
    private int baseAdditionalMultiplier = 1;
    private int additionalMultiplier = 1; 
    private int currentMultiplier = 1;
    [SerializeField] private int maxComboMultiplier;
    [SerializeField] private float scoreIncreaseInterval = 0.5f;

    [SerializeField] private int killsToIncreaseCombo;
    private int currentKills = 0;
    private int previousNoKills = 0;
    private List<DoubleScoreEffect> doubleScoreEffectActiveList = new List<DoubleScoreEffect>();

    private bool isPlayerAlive = true;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentMultiplier = baseMultiplier * additionalMultiplier;
        InvokeRepeating("UpdateScore", scoreIncreaseInterval, scoreIncreaseInterval);

        // Game events
        GameEvents.eventSystem.onEnemyDestroyed += UpdateScoreKilledEnemy;
        GameEvents.eventSystem.onPlayerTakeDamage += ResetScoreMulti;
        GameEvents.eventSystem.onPlayerDeath += UpdatePlayerStatus;
        GameEvents.eventSystem.onMiningRigEmpty += UpdatePlayerStatus;
    }

    public void ModifyAdditionalScoreMultiplier(int value)
    {
        Debug.Log("Moidified additionalScoreMultiplier to: " + value);
        additionalMultiplier = value;
    }

    #region DoubleScorePowerup Stuff
    public void AddToDoubleScoreActiveList(DoubleScoreEffect prefab)
    {
        Debug.Log("Adding a DoubleScoreEffectPrefab");
        doubleScoreEffectActiveList.Add(prefab);
    }

    public void RemoveFromDoubleScoreActiveList(DoubleScoreEffect prefab)
    {
        Debug.Log("Removing a DoubleScoreEffectPrefab");
        doubleScoreEffectActiveList.Remove(prefab);
    }

    public bool IsThereOtherDoubleScoreActive()
    {
        if (doubleScoreEffectActiveList.Count > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion
    public int GetScore()
    {
        return currentScore;
    }

    public int GetCurrentMultiplier()
    {
        currentMultiplier = baseMultiplier * additionalMultiplier;
        return currentMultiplier;
    }

    public int GetBaseAdditionalMultiplier()
    {
        return baseAdditionalMultiplier;
    }

    void UpdateScore()
    {
        // Every frame, add score by a factor
        if (isPlayerAlive == true)
        {
            currentMultiplier = baseMultiplier * additionalMultiplier;
            currentScore += (scoreGrowthFactor * currentMultiplier);
        }
    }

    private void ResetScoreMulti()
    {
        baseMultiplier = 1;
    }

    private void UpdatePlayerStatus()
    {
        isPlayerAlive = false;
    }

    private void UpdateScoreKilledEnemy(int scoreValue)
    {
        // Called whenever an enemy is Killed
        currentMultiplier = baseMultiplier * additionalMultiplier;
        currentScore += (scoreValue * currentMultiplier);
        currentKills++;
        int deltaKills = currentKills - previousNoKills;
        if (deltaKills == killsToIncreaseCombo)
        {
            previousNoKills = currentKills;
            UpdateMultiplier();
        }
    }

    private void UpdateMultiplier()
    {
        baseMultiplier *= 2;
        if (baseMultiplier > maxComboMultiplier)
        {
            baseMultiplier = maxComboMultiplier;
        }
    }
}
