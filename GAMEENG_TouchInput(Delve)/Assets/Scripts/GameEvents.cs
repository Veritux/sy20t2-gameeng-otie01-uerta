﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents eventSystem;

    private void Awake()
    {
        eventSystem = this;
    }

#region input events
    // Tap
    public event Action<Vector2> onTap;
    public void Tap(Vector2 inputPos) { onTap?.Invoke(inputPos); }

    // Swipe
    public event Action<Vector2> onSwipe;
    public void SwipeStart(Vector2 inputPos) { onSwipe?.Invoke(inputPos); }

    public event Action<Vector2> onStillSwiping;
    public void StillSwiping(Vector2 inputPos) { onStillSwiping?.Invoke(inputPos); }
    
    public event Action onSwipeEnd;
    public void SwipeEnd() { onSwipeEnd?.Invoke(); }
    #endregion

    // Player death
    public event Action onPlayerDeath;
    public void PlayerDeath() { onPlayerDeath?.Invoke(); }

    // Player Took damage
    public event Action onPlayerTakeDamage;
    public void PlayerTakeDamage() { onPlayerTakeDamage?.Invoke(); }

    // Mining Rig Fuel
    public event Action onMiningRigEmpty;
    public void MiningRigEmpty() { onMiningRigEmpty?.Invoke(); }

    // Enemy Killed
    public event Action<int> onEnemyDestroyed;
    public void EnemyDestroyed(int score) { onEnemyDestroyed?.Invoke(score); }

    #region Powerups
    public event Action onPowerupTapped;
    public void PowerupTapped() { onPowerupTapped?.Invoke();}

    public event Action<int> onHealthPickup;
    public void HealthPickup(int value) { onHealthPickup?.Invoke(value); }

    public event Action<int> onFuelPickup;
    public void FuelPickup(int value) { onFuelPickup?.Invoke(value); }

    public event Action<float> onDoubleScorePickup;
    public void DoubleScorePickup(float duration) { onDoubleScorePickup?.Invoke(duration); }
    #endregion
}
