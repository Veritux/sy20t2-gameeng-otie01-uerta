﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private int maxHP;
    private int currentHP;

    public int GetMaxHP()
    {
        return maxHP;
    }

    public int GetCurrentHP()
    {
        return currentHP;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }

    public void RestoreHealth(int value)
    {
        Debug.Log("Restored " + value + " health");
        currentHP += value;
        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }
    }

    public void TakeDamage(int damageValue)
    {
        Debug.Log("Received" + damageValue + " damage");
        currentHP -= damageValue;
        if (currentHP <= 0)
        {
            // Call die from owner
            transform.gameObject.GetComponent<Base_Unit>().Die();
        }
    }
}
