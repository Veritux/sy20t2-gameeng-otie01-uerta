﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Subscribe to event
        GameEvents.eventSystem.onPlayerDeath += GameOverNoHealth;
        GameEvents.eventSystem.onMiningRigEmpty += GameOverNoFuel;
    }

    private void GameOverNoHealth()
    {
        Debug.Log("Game over no health");
        SceneManager.LoadScene("GameOver");
    }

    private void GameOverNoFuel()
    {
        Debug.Log("Game over no fuel");
        SceneManager.LoadScene("GameOver");
    }
}
