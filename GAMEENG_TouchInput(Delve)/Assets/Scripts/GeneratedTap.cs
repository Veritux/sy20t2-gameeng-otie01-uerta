﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratedTap : MonoBehaviour
{
    Rigidbody2D rigidBody;
    Camera cam;
    CircleCollider2D circleCol;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        rigidBody = GetComponent<Rigidbody2D>();
        circleCol = GetComponent<CircleCollider2D>();
        circleCol.enabled = false;

        // Listen
        GameEvents.eventSystem.onTap += UpdateTapPosition;
    }

    // Update is called once per frame
    void UpdateTapPosition(Vector2 inputPos)
    {
        circleCol.enabled = true;

        // Move position to latest tapped 
        rigidBody.position = cam.ScreenToWorldPoint(inputPos);

        // Disable collider after some delay 
        StartCoroutine(delayDisable());
    }

    IEnumerator delayDisable()
    {
        yield return new WaitForSeconds(1);
        circleCol.enabled = false;
        Debug.Log("Tap collider disabled");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pickup")
        {
            Debug.Log("A pickup was tapped");
            collision.GetComponentInParent<Powerup>().ExecuteEffect();
        }
    }
}
