﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawnManager : MonoBehaviour
{
    [SerializeField] List<Powerup> listPowerups = new List<Powerup>();
    [SerializeField] private int maxNoPowerups;
    [SerializeField] private float minSpawnInterval;
    [SerializeField] private float maxSpawnInterval;

    private float spawnInterval = 0;

    private int currentNoPowerups = 0;
    private SpawnZone spawnZone;

    // Start is called before the first frame update
    void Start()
    {
        spawnZone = transform.GetComponentInChildren<SpawnZone>();

        if (spawnZone == null)
        {
            Debug.LogError("NO SPAWN ZONE!");
        }

        spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
        InvokeRepeating("SpawnPowerup", 5.0f, spawnInterval); // powerups start to spawn 10 seconds into the game

        //Listen
        GameEvents.eventSystem.onPlayerDeath += StopSpawner;
        GameEvents.eventSystem.onMiningRigEmpty += StopSpawner;
        GameEvents.eventSystem.onPowerupTapped += UpdatePowerupCount;
    }

    private void StopSpawner()
    {
        Debug.LogWarning("Spawnin of powerups stopped");
        CancelInvoke("SpawnPowerup");
    }

    private void UpdatePowerupCount()
    {
        Debug.LogWarning("Powerup count --");
        currentNoPowerups--;
    }

    void SpawnPowerup()
    {
       // Debug.Log("Hi I'm in the SpawnPowerup()");

        if (currentNoPowerups >= maxNoPowerups)
        {
            Debug.LogError("current number of powerups >= maxNoPowerups");
            spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
            return;
        }
            int randomIndex = Random.Range(0, listPowerups.Count);

            Vector3 spawnLocation = spawnZone.GetRandomPoint();
            GameObject newPowerup = ObjectPooler.instance.SpawnFromPool(listPowerups[randomIndex].name, spawnLocation, Quaternion.identity);
           // Debug.LogWarning("A powerup should have spawned");
            currentNoPowerups++;
           // Debug.LogWarning("Current powerup count: " + currentNoPowerups);

            spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
            //Debug.LogWarning("Next powerup should spawn in: " + spawnInterval);
    }
}
