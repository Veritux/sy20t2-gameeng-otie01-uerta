﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScoreEffect : MonoBehaviour
{
    private int scoreMultiplierAdditional = 2;
    private ScoreManager scoreManagerInstance;
    public void Init(float nLifetime)
    {
        Debug.Log("In init of DoubleScoreEffect");
        scoreManagerInstance = ScoreManager.instance;
        StartCoroutine(ExecuteEffect(nLifetime));
    }

    IEnumerator ExecuteEffect(float lifetime)
    {
        Debug.Log("Inside the ExecuteEffect ofDoubleScoreEffect");
        scoreManagerInstance.ModifyAdditionalScoreMultiplier(scoreManagerInstance.GetBaseAdditionalMultiplier() * scoreMultiplierAdditional);
        scoreManagerInstance.AddToDoubleScoreActiveList(this);
        yield return new WaitForSeconds(lifetime);

        Debug.Log("After the lifetime of DoubleScoreEffect");
        if (!scoreManagerInstance.IsThereOtherDoubleScoreActive())
        {
            scoreManagerInstance.ModifyAdditionalScoreMultiplier(scoreManagerInstance.GetBaseAdditionalMultiplier());
        }
        scoreManagerInstance.RemoveFromDoubleScoreActiveList(this);
        Destroy(gameObject);
    }
}
