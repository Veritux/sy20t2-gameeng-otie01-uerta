﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelPowerup : Powerup
{
    [SerializeField] private int fuelToRestore;
    public override void Effect()
    {
        GameEvents.eventSystem.FuelPickup(fuelToRestore);
    }
}
