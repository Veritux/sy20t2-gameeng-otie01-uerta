﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScorePowerup : Powerup
{
    [SerializeField] private float duration;
    [SerializeField] private GameObject doubleScoreEffectPrefab;

    public override void Effect()
    {
        GameEvents.eventSystem.DoubleScorePickup(duration);
        GameObject newDoubleScoreEffect = Instantiate(doubleScoreEffectPrefab, transform.localScale, Quaternion.identity);
        newDoubleScoreEffect.GetComponent<DoubleScoreEffect>().Init(duration);
    }
}
