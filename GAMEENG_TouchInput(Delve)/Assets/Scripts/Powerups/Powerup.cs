﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    [SerializeField] private string powerupName;
    public void ExecuteEffect()
    {
        Effect();
        GameEvents.eventSystem.PowerupTapped();
        gameObject.SetActive(false);
    }

    public virtual void Effect()
    {

    }
}
