﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerup : Powerup
{
    [SerializeField] private int healthRestored;

    public override void Effect()
    {
        GameEvents.eventSystem.HealthPickup(healthRestored);
    }
}
