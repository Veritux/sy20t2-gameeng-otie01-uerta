﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZone : MonoBehaviour
{
    [SerializeField] private List<Transform> spawnLocations = new List<Transform>();

    public Vector3 GetRandomPoint()
    {
        Vector2 bounds;
        Vector3 spawnLocation = new Vector3(0, 0, 0);

        Transform chosenSpawnBox = spawnLocations[Random.Range(0, spawnLocations.Count)];
        bounds = chosenSpawnBox.GetComponent<BoxCollider2D>().bounds.size;
        bounds.x /= 2;
        bounds.y /= 2;

        spawnLocation.x = chosenSpawnBox.position.x + Random.Range(-bounds.x, bounds.x);
        spawnLocation.y = chosenSpawnBox.position.y + Random.Range(-bounds.y, bounds.y);

        return spawnLocation;
    }
}
