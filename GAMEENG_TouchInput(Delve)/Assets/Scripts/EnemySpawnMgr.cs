﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnMgr : MonoBehaviour
{
    [SerializeField] List<Enemy> enemyPrefabs = new List<Enemy>();
    private List<Transform> spawnBoxes = new List<Transform>();
    [SerializeField] private Player thePlayer;
    [SerializeField] private float minSpawnInterval;
    [SerializeField] private float maxSpawnInterval;

    private float spawnInterval = 0;

    ObjectPooler objectPooler;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
            spawnBoxes.Add(child);
        }

        objectPooler = ObjectPooler.instance;

        spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
        InvokeRepeating("SpawnEnemy", 5.0f, spawnInterval);

        GameEvents.eventSystem.onPlayerDeath += StopSpawner;
        GameEvents.eventSystem.onMiningRigEmpty += StopSpawner;
    }

    private void StopSpawner()
    {
        CancelInvoke("SpawnEnemy");
    }

    private void SpawnEnemy()
    {
        int randomIndex = Random.Range(0, enemyPrefabs.Count);
        Vector2 bounds;
        Vector3 spawnLocation = new Vector3(0, 0, thePlayer.transform.position.z);

        Transform chosenSpawnBox = spawnBoxes[Random.Range(0, spawnBoxes.Count)];
        bounds = chosenSpawnBox.GetComponent<BoxCollider2D>().bounds.size;
        bounds.x /= 2;
        bounds.y /= 2;

        spawnLocation.x = chosenSpawnBox.position.x + Random.Range(-bounds.x, bounds.x);
        spawnLocation.y = chosenSpawnBox.position.y + Random.Range(-bounds.y, bounds.y);

        //Enemy newEnemy = Instantiate(enemyPrefabs[randomIndex], spawnLocation, Quaternion.identity);
        //newEnemy.Init(thePlayer);

        // Object pooling spawn
        GameObject newEnemy = objectPooler.SpawnFromPool(enemyPrefabs[randomIndex].name, spawnLocation, Quaternion.identity);
        newEnemy.GetComponent<Enemy>().Init(thePlayer);

        spawnInterval = Random.Range(minSpawnInterval, maxSpawnInterval);
        // Debug.LogWarning("Next Enemy should spawn in: " + spawnInterval);
    }
}
