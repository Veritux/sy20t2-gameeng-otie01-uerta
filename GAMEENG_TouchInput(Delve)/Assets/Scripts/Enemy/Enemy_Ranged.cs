﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Ranged : Enemy
{
    [SerializeField] EnemyBullet bulletPrefab;

    protected override void Attack()
    {
        // Different behavior, will spawn a missle thingy instead
        Quaternion spawnRotation = transform.rotation * Quaternion.Euler(1, 1, -90);
        //EnemyBullet newEnemyBullet = Instantiate(bulletPrefab, transform.position, spawnRotation);

        GameObject newEnemyBullet = ObjectPooler.instance.SpawnFromPool(bulletPrefab.name, transform.position, spawnRotation);
        //Debug.LogWarning("Bullet should have spawned");
        newEnemyBullet.GetComponent<EnemyBullet>().Init(damage);
        //newEnemyBullet.GetComponent<EnemyBullet>().StartDeathTimer();

        //Quaternion bulletRotation = transform.rotation * Quaternion.Euler(0, 0, -90);
        //newEnemyBullet.transform.rotation *= bulletRotation;

        //newEnemyBullet.Init(damage);
    }
}
