﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HealthComponent))]
public class Enemy : Base_Unit, IPooledObject
{
    [SerializeField] protected float speed;
    [SerializeField] protected float range;
    [SerializeField] protected int damage;
    [SerializeField] protected float attackSpeed;
    [SerializeField] protected int scoreAmount;

    private HealthComponent healthComponent;
    private Player playerReference = null; // Given by enemy spawn manager
    private bool isAttacking = false;


    // Start is called before the first frame update
    //void Start()
    public void OnObjectSpawn()
    {
        healthComponent = GetComponent<HealthComponent>();
    }

    public void Init(Player thePlayer)
    {
        playerReference = thePlayer;
    }

    // Update is called once per frame
    void Update()
    {
        ExecuteBehavior();    
    }

    public HealthComponent GetHealthComponent()
    {
        return healthComponent;
    }

    private void ExecuteBehavior()
    {
        // Keep on facing the direction of the player
        RotateTowardsPlayer();

        if (Vector3.Distance(transform.position, playerReference.transform.position) > range)
        {
            if (isAttacking == true)
            {
                CancelInvoke("Attack");
                isAttacking = false;
            }
            MoveTowardsPlayer();
        }
        else
        {
            if (isAttacking == false)
            {
                InvokeRepeating("Attack", 0f, attackSpeed);
                isAttacking = true;
            }
        }
    }

    private void MoveTowardsPlayer()
    {
        Transform goal = playerReference.transform;
        Vector2 direction = goal.position - transform.position;

        transform.position = Vector3.MoveTowards(transform.position, goal.position, speed * Time.deltaTime);
        //this.transform.position = Vector3.Lerp(transform.position, goal.transform.position, Time.deltaTime * speed);
    }

    private void RotateTowardsPlayer()
    {
        Transform goal = playerReference.transform;
        Vector2 direction = goal.position - transform.position;

        // 2D rotation
        float rotSpeed = 0.8f;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward); // around the Z axis
        this.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotSpeed);

        //Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        //transform.rotation = rotation;
    }

    protected virtual void Attack()
    {
        // Default attack is that of melee units, the player just takes damage
        Debug.Log("An enemy attacked!");
        playerReference.TakeDamage(damage);
    }

    public override void Die()
    {
        Debug.Log("An enemy was destroyed!");
        GameEvents.eventSystem.EnemyDestroyed(scoreAmount);

        // Instead of dying, disable? 
        //Destroy(this.gameObject);

        // Disable attacking
        CancelInvoke("Attack");
        isAttacking = false;
        gameObject.SetActive(false);
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == "laser" )
    //    {
    //        Debug.Log("An enemy was swiped");
    //        healthComponent.TakeDamage(1); // swipe damage fixed at 1
    //    }
    //}
}
