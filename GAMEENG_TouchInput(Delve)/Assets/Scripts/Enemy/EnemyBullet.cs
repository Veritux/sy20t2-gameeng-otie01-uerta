﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float lifetime;

    private int damage = 0;

    public void Init(int nDamage)
    {
        damage = nDamage;
    }
    public void StartDeathTimer()
    {
        // NOT USED ATM, refer to Start() instead

        // Destroy(this.gameObject, lifetime);
        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        //Destroy(this.gameObject, lifetime);
        StartCoroutine(disableAfterSeconds(lifetime));
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate((Vector3.up) * bulletSpeed * Time.deltaTime); // 
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponentInParent<Player>())
        {
            col.gameObject.GetComponentInParent<Player>().TakeDamage(damage);
            Debug.Log("BULLET hit PLAYER");
            // Destroy(this.gameObject);
            gameObject.SetActive(false);
        }
    }

    IEnumerator disableAfterSeconds(float lifetime)
    {
        yield return new WaitForSeconds(lifetime); // not sure if a float passed to WaitForSeconds will work
        gameObject.SetActive(false);
    }
}
