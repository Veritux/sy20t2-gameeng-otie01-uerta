﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSwipe : MonoBehaviour
{
    public GameObject laserTrailPrefab; // just aesthetics (the trail)
    GameObject currentLaserTrail;

    Rigidbody2D rigidBody;
    Camera cam;
    CircleCollider2D circleCol;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        rigidBody = GetComponent<Rigidbody2D>();
        circleCol = GetComponent<CircleCollider2D>();

        // Subscribe to event
        // Will receive info from TouchListener through events if TouchListener recognizes input as a SWIPE. 
        GameEvents.eventSystem.onSwipe += StartLaser;
        GameEvents.eventSystem.onStillSwiping += UpdateLaser;
        GameEvents.eventSystem.onSwipeEnd += EndLaser;
    }
    private void StartLaser(Vector2 inputPos)
    {
        // rigidBody.position = cam.ScreenToWorldPoint(inputPos);
        currentLaserTrail = Instantiate(laserTrailPrefab, transform);
        circleCol.enabled = true;
    }

    private void UpdateLaser(Vector2 inputPos)
    {
        // Update the position of the laser 
        rigidBody.position = cam.ScreenToWorldPoint(inputPos); 
    }

    private void EndLaser()
    {
        currentLaserTrail.transform.SetParent(null);
        Destroy(currentLaserTrail, 1.5f);
        circleCol.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.GetComponent<Enemy>().GetHealthComponent().TakeDamage(1);  // swipe damage fixed at 1
            Debug.Log("An enemy was swiped");
        }
    }
}
