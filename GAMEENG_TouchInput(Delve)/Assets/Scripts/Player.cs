﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Base_Unit
{
    [SerializeField] private int maxFuel;
    private int currentFuel;

    [SerializeField] private int fuelDecreaseAmount;
    [SerializeField] private float fuelDecreaseInterval;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private FuelBar fuelBar;

    private HealthComponent healthComponent;
    private bool isAlive = true;
    private bool hasFuel = true;

    // Start is called before the first frame update
    void Start()
    {
        healthComponent = GetComponent<HealthComponent>();

        currentFuel = maxFuel;
        InvokeRepeating("UpdateFuel", fuelDecreaseInterval, fuelDecreaseInterval);

        healthBar.SetMaxHealth(healthComponent.GetMaxHP());
        fuelBar.SetMaxFuel(currentFuel);

        // Listen
        GameEvents.eventSystem.onHealthPickup += RestoreHealth;
        GameEvents.eventSystem.onFuelPickup += RestoreFuel;
    }

    private void RestoreHealth(int value)
    {
        healthComponent.RestoreHealth(value);
    }

    private void RestoreFuel(int value)
    {
        currentFuel += value;
        if (currentFuel > maxFuel)
        {
            currentFuel = maxFuel;
        }
        fuelBar.SetFuel(currentFuel);
    }

    public void TakeDamage(int damageValue)
    {
        GameEvents.eventSystem.PlayerTakeDamage();
        Debug.Log("Player received: " + damageValue);
        healthComponent.TakeDamage(damageValue);
        healthBar.SetHealth(healthComponent.GetCurrentHP());
    }

    private void UpdateFuel()
    {
        currentFuel -= fuelDecreaseAmount;
        fuelBar.SetFuel(currentFuel);
        if (currentFuel <= 0 && hasFuel == true)
        {
            hasFuel = false;
            Debug.Log("Mining rig out of fuel!");
            GameEvents.eventSystem.MiningRigEmpty();
        }
    }

    public override void Die()
    {
        if (isAlive == true)
        {
            Debug.Log("Mining rig out of health!");
            GameEvents.eventSystem.PlayerDeath();
            CancelInvoke("UpdateFuel");
            Debug.Log("Stopping fuel tick");
            isAlive = false;
        }
    }
}
