﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text multiplierText;

    // Start is called before the first frame update
    void Start()
    {
        //scoreText.text = "Score: " + ScoreManager.instance.GetScore().ToString();
        //multiplierText.text = "Combo: " + ScoreManager.instance.GetCurrentMultiplier().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + ScoreManager.instance.GetScore().ToString();
        multiplierText.text = "Combo: " + ScoreManager.instance.GetCurrentMultiplier().ToString();
    }
}
